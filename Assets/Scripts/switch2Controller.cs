﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switch2Controller : MonoBehaviour
{

    public GameObject effectedObject;
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            effectedObject.SetActive(true);
            Debug.Log("bullet hit");
        }
    }
}
