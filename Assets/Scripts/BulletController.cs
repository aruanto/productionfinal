﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class BulletController : MonoBehaviour
{
    Vector3 moveDirection;
    public float speed;
    public GameObject self;
    void Start()
    {
        moveDirection = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position);
        moveDirection.z = 0;
        moveDirection.Normalize();
    }

    void Update()
    {
        transform.position = transform.position + moveDirection * speed * Time.deltaTime;
        transform.localScale = new Vector3(1, 1, 1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || (collision.gameObject.CompareTag("Ground")))
        {
            Destroy(self);
        }
    }
}