﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb2D;
    public GameObject gameCamera;
    public GameObject scaleReset;
    public Text timer;
    public Text endText;
    public Transform playerTransform;
    public float jumpForce = 425;
    public float speed = 30;
    public int jumpCount = 2;
    public Vector3 playerVelocity;
    public GameObject bullet;
    public Vector3 checkpoint;

    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        playerTransform = GetComponent<Transform>();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        checkpoint = playerTransform.position;
    }

    //Acts as a ground check
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            jumpCount = 2;
            Debug.Log("touched ground");
        }
        if (collision.gameObject.CompareTag("Killbox"))
        {
            playerTransform.position = checkpoint;
            Debug.Log("touched killbox");
            rb2D.velocity = new Vector2(0, 0);
            timer.GetComponent<timerController>().TimerPenalty();
        }
        if (collision.gameObject.CompareTag("Checkpoint"))
        {
            checkpoint = collision.gameObject.transform.position;
            Destroy(collision.gameObject);
            Debug.Log("touched checkpoint");
        }
        if (collision.gameObject.CompareTag("End"))
        {
            Debug.Log("touched end");
            speed = 0;
            jumpForce = 0;
            timer.GetComponent<timerController>().CalculateScore();
            rb2D.velocity = new Vector2(0, 0);
        }
    }
    void FixedUpdate()
    {
        //All of this controls movement
        bool jumpInput = Input.GetKeyDown("space");
        if (jumpInput & jumpCount > 0)
        {
            rb2D.AddForce(new Vector2(0, jumpForce));
            jumpCount = jumpCount - 1;
        }
        bool leftInput = Input.GetKey("a");
        if (leftInput & playerVelocity.x > -30)
        {
            rb2D.AddForce(new Vector2(-speed, 0));
        }
        bool rightInput = Input.GetKey("d");
        if (rightInput & playerVelocity.x < 30)
        {
            rb2D.AddForce(new Vector2(speed, 0));
        }
        bool crouchInput = Input.GetKey("s");
        if (crouchInput)
        {
            playerTransform.localScale = new Vector3(1, 0.35f, 1);
        }
        if (crouchInput == false)
        {
            playerTransform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
        }
        playerVelocity = rb2D.velocity;
        if (playerVelocity.x >= 30 & rightInput)
        {
            playerVelocity.x = 30;
        }
        if (playerVelocity.x <= -30 & leftInput)
        {
            playerVelocity.x = -30;
        }

        //Moves camera to player X but offset
        gameCamera.transform.position = new Vector3(playerTransform.position.x + 10, 3, -10);

        //For player shooting
        bool mouseClick = Input.GetKeyDown(KeyCode.Mouse0);
        if (mouseClick)
        {
            Debug.Log("detected click");
            Instantiate(bullet, playerTransform.position, scaleReset.transform.rotation, scaleReset.transform);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            timer.GetComponent<timerController>().TimerPenalty();
            Destroy(collision.gameObject);
        }
    }
}
