﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class enemyController : MonoBehaviour
{
    public GameObject self;
    public GameObject timer;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            timer.GetComponent<timerController>().enemyCount = timer.GetComponent<timerController>().enemyCount - 1f;
            Destroy(self);
        }
    }
}
