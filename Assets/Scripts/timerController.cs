﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class timerController : MonoBehaviour
{
    public float seconds = 0f;
    public float minutes = 0f;
    public float enemyCount = 10f;
    public float enemyPenalty;
    public Text timer;
    public Text tutorialText;
    public Text endText;
    public bool timerActive;
    public float score;
    public string rating;

    // Start is called before the first frame update
    void Start()
    {
        timer = GetComponent<Text>();
        timerActive = true;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
       if (timerActive)
        {
            seconds = seconds + Time.deltaTime;
            if (seconds >= 60)
            {
                minutes = minutes + 1f;
                seconds = seconds - 60f;
            }
            timer.text = minutes + ":" + seconds.ToString("00") + "s";
        }
    }
    public void TimerPenalty()
    {
        seconds = seconds + 30;
    }
    public void CalculateScore()
    {
        enemyPenalty = enemyCount * 30f;
        tutorialText.text = "Press R to restart level. Enemies missed: " + enemyCount;
        timerActive = false;
        minutes = minutes * 60f;
        score = minutes + seconds + enemyPenalty;
        if (score <= 60)
        {
            rating = "SSS";
        }
        if (score <= 90 & score >= 61)
        {
            rating = "SS";
        }
        if (score <= 120 & score >= 91)
        {
            rating = "S";
        }
        if (score <= 150 & score >= 121)
        {
            rating = "A";
        }
        if (score <= 180 & score >= 151)
        {
            rating = "B";
        }
        if (score <= 210 & score >= 181)
        {
            rating = "C";
        }
        if (score <= 240 & score >= 211)
        {
            rating = "D";
        }
        if (score <= 270 & score >= 241)
        {
            rating = "E";
        }
        if (score >= 271)
        {
            rating = "F";
        }
        endText.text = "Level Complete! Rating: " + rating;
    }

}
